install.packages(c("tidyverse", "car", "compute.es", "ez", "multcomp", "pastecs", "WRS2"))
library(car)
library(compute.es)
library(multcomp)
library(pastecs)
library(WRS2)
library(ez)
library(tidyverse)

#home dir
setwd("E:\\repositories\\masters\\RSyntax\\scripts")
#laptop dir
# setwd("C:\\Users\\fauxphox\\Documents\\repositories\\masters\\RSyntax\\scripts")
#lab dir
#setwd("~/repositories/masters/RSyntax/scripts")

# completed the tasks work
# read the csv
# set lang as factors with levels 1= basic syntax, 2= tilda syntax, 3= tidy syntax
myData<-read_csv("allData.csv")
myData$assignedStyle<-as.factor(myData$assignedStyle)
levels(myData)<-c('rBasic', 'tilda', 'tidy')

# find the discrepancy from the database (n=206) and SQL query (n=182)
myData2<-read.csv("participants.csv")
# get initial id count
uniqueId<-distinct(myData,id)
# ids that are not present after SQL query
# id list 21,22,37,47,55,74,75,90,69,102,108,112,120,155,161,165,168,173,189,192,196,200,216,223

myData2<-rbind(myData2,uniqueId)

#####################################################################################################
# clean the data by removing outliers 

# Step1: Remove tasks 3 and 7 due to errors in instruction sheet 

groupByMajorFilter37<-myData%>%filter((task==3)==FALSE)
myCleanDataStep1<-groupByMajorFilter37%>%filter((task==7)==FALSE)

# Step2:  Remove codeEvent==22 these are checks not final times
myCleanDataStep2<-myCleanDataStep1%>%filter(codeEvent!=22)
#myCleanData<-myCleanData%>%filter(Time < 1300)%>%filter(codeEvent!=22)
uniqueId<-distinct(myCleanDataStep2,id, courseNum)

# Step3:  remove 302 students with 3+ years experience
remove302<-c(18,24,31,34,36)

myCleanDataStep3<-myCleanDataStep2[ ! myCleanDataStep2$id %in% remove302, ]

uniqueId<-distinct(myCleanDataStep3,id)

# break up code to look at missing values in two parts and add >3 missing entries into invalid set
#small<-myCleanDataStep3%>%filter(id< 111)
#ezDesign(small,x=id,y=task)

#big<-myCleanDataStep3%>%filter(id>=111)
#ezDesign(big,x=id,y=task)

#ezDesign(myCleanDataStep3,x=id,y=task)

# Step4: remove invalid set

removeId<-c(20,33,41,44,46,57,65,85,93,99,103,107,109,123,148,159,160,171,174,178,188,202,212)

myCleanDataStep4<-myCleanDataStep3[ ! myCleanDataStep3$id %in% removeId, ]

uniqueId<-distinct(myCleanDataStep4,id)

# Step5: create a new column time that mutates Time values > 1300 to max time of 1200 else keeps current value

myCleanDataStep5<-mutate(myCleanDataStep4,Time=ifelse(Time>1200,1200,Time))

findmultiple<-myCleanDataStep5%>%filter(id==91|id==101|id==116|id==121|id==122|id==169|id==203|id==205)

# find double and triple values by visual inspection and remove those entries
# rule is to keep only the lowest identifier value

# id task
# filter by indentifier 

# 91 6
myCleanDataStep5<-myCleanDataStep5%>%filter(identifier!=75993)

# 101 4 there are two of these
myCleanDataStep5<-myCleanDataStep5%>%filter(identifier!=81731)
myCleanDataStep5<-myCleanDataStep5%>%filter(identifier!=81732)

#116 2,4,8,9 
myCleanDataStep5<-myCleanDataStep5%>%filter(identifier!=97354)
myCleanDataStep5<-myCleanDataStep5%>%filter(identifier!=98160)
myCleanDataStep5<-myCleanDataStep5%>%filter(identifier!=99388)
myCleanDataStep5<-myCleanDataStep5%>%filter(identifier!=99844)

# 121 0
myCleanDataStep5<-myCleanDataStep5%>%filter(identifier!=101868)

# 122 1
myCleanDataStep5<-myCleanDataStep5%>%filter(identifier!=103889)

# 169 0
myCleanDataStep5<-myCleanDataStep5%>%filter(identifier!=164435)

# 203 5
myCleanDataStep5<-myCleanDataStep5%>%filter(identifier!=196533)

# 205 0
myCleanDataStep5<-myCleanDataStep5%>%filter(identifier!=196188)

#visually inspect the dataset

ezDesign(myCleanDataStep5,id, task)

# Step6 add in max time for ids with missing values

# if a row is missing for a task enter a max time value

#28
myCleanDataStep6<-add_row(myCleanDataStep5, id=28,task=8,Time=1200)
myCleanDataStep6<-add_row(myCleanDataStep6, id=28,task=9,Time=1200)
#58
myCleanDataStep6<-add_row(myCleanDataStep6, id=58,task=8,Time=1200)
myCleanDataStep6<-add_row(myCleanDataStep6, id=58,task=9,Time=1200)
#89
myCleanDataStep6<-add_row(myCleanDataStep6, id=89,task=6,Time=1200)
myCleanDataStep6<-add_row(myCleanDataStep6, id=89,task=8,Time=1200)
myCleanDataStep6<-add_row(myCleanDataStep6, id=89,task=9,Time=1200)
#101
myCleanDataStep6<-add_row(myCleanDataStep6, id=101,task=5,Time=1200)
myCleanDataStep6<-add_row(myCleanDataStep6, id=101,task=9,Time=1200)
#122
myCleanDataStep6<-add_row(myCleanDataStep6, id=122,task=8,Time=1200)
myCleanDataStep6<-add_row(myCleanDataStep6, id=122,task=9,Time=1200)
#130
myCleanDataStep6<-add_row(myCleanDataStep6, id=130,task=8,Time=1200)
#156
myCleanDataStep6<-add_row(myCleanDataStep6, id=156,task=8,Time=1200)
myCleanDataStep6<-add_row(myCleanDataStep6, id=156,task=9,Time=1200)
#164
myCleanDataStep6<-add_row(myCleanDataStep6, id=164,task=6,Time=1200)
myCleanDataStep6<-add_row(myCleanDataStep6, id=164,task=8,Time=1200)
myCleanDataStep6<-add_row(myCleanDataStep6, id=164,task=9,Time=1200)
#166
myCleanDataStep6<-add_row(myCleanDataStep6, id=166,task=8,Time=1200)
myCleanDataStep6<-add_row(myCleanDataStep6, id=166,task=9,Time=1200)
#169
myCleanDataStep6<-add_row(myCleanDataStep6, id=169,task=8,Time=1200)
myCleanDataStep6<-add_row(myCleanDataStep6, id=169,task=9,Time=1200)
#182
myCleanDataStep6<-add_row(myCleanDataStep6, id=182,task=6,Time=1200)
myCleanDataStep6<-add_row(myCleanDataStep6, id=182,task=8,Time=1200)
myCleanDataStep6<-add_row(myCleanDataStep6, id=182,task=9,Time=1200)
#210
myCleanDataStep6<-add_row(myCleanDataStep6, id=210,task=9,Time=1200)

# write to a csv to manually copy missing row values (this probably can be done programatically)

write.csv(myCleanDataStep6,"fixNAValuesStep6a.csv")

#!/bin/python3

import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt

def makeCorrelationMatrix (df):
    df_corr = df.copy()

    df_corr = df_corr[
            (df_corr['method or variable name from sample page'] != -1) &
            (df_corr['incorrect variable name from the table (or in the wrong place)'] != -1) &
            (df_corr['extra characters'] != -1) &
            (df_corr['extra lines of code'] != -1) &
            (df_corr['raw numerical data'] != -1)
            ]

    corr = df_corr.corr(method='pearson')

    mask = np.zeros_like(corr, dtype=np.bool)
    mask[np.triu_indices_from(mask)] = True

    f, ax = plt.subplots(figsize=(50, 50))

    plot = sns.heatmap(corr, mask=mask, vmax=1, 
    			center=0, annot=True, fmt='.1f',
	            square=True, linewidths=.5, 
	            cbar_kws={"shrink": .5});
    f.savefig("output/error_correlation.eps")
    return corr

def compareErrors(df, column, name):
    true_column = name+"_true"
    false_column = name+"_false"
    
    group_true = df[df[column]==1] \
       .groupby(['variable'])['value'].sum() \
       .to_frame(name = 'sum').sort_values(by='sum',ascending=False)
    
    group_false = df[df[column]==0] \
       .groupby(['variable'])['value'].sum()  \
       .to_frame(name = 'sum').sort_values(by='sum',ascending=False)
    
    group_true.columns = [true_column]
    group_false.columns = [false_column]
    
    group_combined = pd.concat([group_true, group_false], \
    	axis=1, sort=True)
    
    true_sum = group_combined[true_column].sum()
    false_sum = group_combined[false_column].sum()

    group_combined['true_per'] = group_combined[true_column] / true_sum
    group_combined['false_per'] = group_combined[false_column] / false_sum
    
    return group_combined.sort_values(by=true_column, ascending=False)


df = pd.read_csv('joined.csv')

columns = df.loc[:, 'argument empty': \
    'wrong use of tilde operator (must have both sides)'].columns

columns_with = df.loc[:, 'argument empty':'sum_errors'].columns

idVars = df.loc[:, :'raw numerical data'].columns
idVars

qualVars = df.loc[:,'method or variable name from sample page':'raw numerical data'].columns

short_names = ['sample', 'variablename', 'extrachars', 'extralines', 'rawnum']

correlations = makeCorrelationMatrix(df)

for i, column_name in enumerate(qualVars):
    sorted_corr = correlations[column_name].sort_values(ascending=False)
    sorted_corr.to_csv('output/' + short_names[i] + '_corr.csv', header=True)
    sorted_corr.head(15).to_latex('output/' +short_names[i] + '_corr.tex', header=True)

df_long = pd.melt(df, id_vars=idVars, value_vars=columns.tolist())

df_long.to_csv("joined_long.csv")
comparisons = []
for i, column_name in enumerate(qualVars):
    comparisons.append(compareErrors(df_long, column_name, short_names[i]))
    comparisons[i].to_csv('output/' + short_names[i]+'.csv')
    comparisons[i].head(15).to_latex('output/' + short_names[i]+'.tex')


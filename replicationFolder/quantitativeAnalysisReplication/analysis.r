install.packages(c("tidyverse", "car", "compute.es", "multcomp", "pastecs", "WRS2"))

library(car)
library(compute.es)
library(multcomp)
library(pastecs)
library(WRS2)
library(tidyverse)
library(ez)

# This needs to be set to the right folder
setwd("~/Google Drive File Stream/My Drive/Lab/Studies/RSyntax/Quantitative Errors/replication/")

d <- read.csv("joined.csv")

d.long <- read.csv("joined_long.csv")

names(d) <- tolower(names(d))
names(d.long) <- tolower(names(d.long))

d$id <- factor(d$id)
d$task <- factor(d$task)

d$assignedstyle.x <- factor(d$assignedstyle.x)
d.long$assignedstyle.x <- factor(d.long$assignedstyle.x)
levels(d$assignedstyle.x)<-c("base R","tilde","tidyverse")
levels(d.long$assignedstyle.x)<-c("base R","tilde","tidyverse")

levels(d$task)<-c(1,2,4,5,6,8,9,10)
levels(d.long$task)<-c(1,2,4,5,6,8,9,10)


model <- ezANOVA(data=d,
                 dv= .(sum_errors),
                 wid= .(id),
                 between=.(assignedstyle.x, groupedmajor),
                 within=.(task),
                 detailed=TRUE,
                 type=3)
model

# Time event numbers
d %>% group_by(groupedmajor) %>% summarize(count = n(), total=1232) %>% mutate(freq=round((count/total)*100,2))
d %>% group_by(assignedstyle.x) %>% summarize(count = n(), total=1232) %>% mutate(freq=round((count/total)*100,2))

d.numbers <- d %>% mutate(allerrors=sum(sum_errors))

d.numbers %>% group_by(assignedstyle.x) %>% summarize(errorevents=sum(sum_errors), allerrors=max(allerrors), mean=mean(sum_errors), sd=sd(sum_errors)) %>% mutate(freq=round((errorevents/allerrors) *100, 2))

d.numbers %>% group_by(groupedmajor) %>% summarize(errorevents=sum(sum_errors), allerrors=max(allerrors), mean=mean(sum_errors), sd=sd(sum_errors)) %>% mutate(freq=round((errorevents/allerrors) *100, 2))

# filter out -1's because they are not meaningful
d.desc <- d %>% filter(method.or.variable.name.from.sample.page != -1 &
                         incorrect.variable.name.from.the.table..or.in.the.wrong.place. != -1 &
                         extra.characters != -1 &
                         extra.lines.of.code != -1 &
                         raw.numerical.data != -1)

d.desc %>% summarise( avg = mean(sum_errors), max = max(sum_errors), min = min(sum_errors), sd = sd(sum_errors))

d.desc %>% group_by (assignedstyle.x) %>% summarise( avg = mean(sum_errors) , max = max(sum_errors), min = min(sum_errors), sd= sd(sum_errors))

d.desc %>% group_by (assignedstyle.x, id) %>% summarise( sum_errors = sum(sum_errors)) %>% summarise( avg = mean(sum_errors), sd=sd(sum_errors))

d.desc %>% group_by (id) %>% summarise( sum_errors = sum(sum_errors)) %>% summarize (avg=mean(sum_errors), sd=sd(sum_errors))
# find all errors of the data and rank them with number of occurrances
d.long %>%
  group_by(variable) %>%
  summarise(mean=mean(value), count=sum(value)) %>%
  arrange(desc(count)) %>%
  print(n= Inf)

nrow(d.long)

# occurances of errors by group, ranked. For graph or table
d.summarized <- d.long %>% 
  group_by(variable, assignedstyle.x) %>% 
  summarise(count=sum(value)) %>% 
  group_by(assignedstyle.x) %>% 
  mutate(freq = round((count/sum(count)*100),2)) %>% 
  arrange(desc(count), .by_group=TRUE) %>% 
  top_n (n= 4, wt= freq) %>% 
  print(n = Inf)

ggplot (d.summarized, aes(x=reorder(variable, -freq), y=freq, fill=assignedstyle.x)) + 
  geom_bar(width = 0.75, stat="identity", position=position_dodge(), colour="black") + 
  scale_fill_grey(start=0.6, end=0.9) +
  theme(legend.position="bottom", text = element_text(size=24), axis.text.x = element_text(angle = 10, hjust = 1, size =20)) +
  labs(x="Error", y = "Frequency", fill="Group") +
  ylim(0, 60)

ggsave("graphs/mostcommonerrors.eps", width=16, height=6)

# facet grid error
ggplot(d, aes(x=groupedmajor, y=sum_errors, fill=groupedmajor)) + 
  geom_boxplot() + 
  facet_grid(assignedstyle.x ~ task) + 
  labs(x = "Major", y = "Number of Errors", fill="Major") + 
  scale_fill_grey(start=0.6, end=0.9) +
  theme(legend.position="bottom",text = element_text(size=24)) +
  ylim(0, 30)

ggsave("graphs/error_facet_grid.eps", width=16, height=6)

d.qual <- d.desc %>% 
  gather(variable, value, method.or.variable.name.from.sample.page:raw.numerical.data) %>% 
  select(id, task, assignedstyle.x, groupedmajor, variable, value)

# overall qual error numbers
d.qual %>% group_by( variable) %>% 
  summarize(sum=sum(value), count=n(), sd=sd(value), mean=mean(value)) %>% 
  mutate(freq=round((sum/count)*100,2))


d.qual %>% group_by(groupedmajor) %>% summarize(count = n()/5, total = 1112) %>% mutate(freq=round((count/total)*100,2))
d.qual %>% group_by(assignedstyle.x) %>% summarize(count = n()/5, total = 1112)%>% mutate(freq=round((count/total)*100,2))

d.qual$variable <- factor(d.qual$variable)
d.qual$id <- factor(d.qual$id)
model.qual <- ezANOVA(data=d.qual,
                 dv= .(value),
                 wid= .(id),
                 between=.(assignedstyle.x,variable, groupedmajor),
                 detailed=TRUE,
                 type=3)
model.qual

d.qual.graph <- d.qual %>% 
  group_by(assignedstyle.x, variable, groupedmajor) %>% 
  summarize(sum=sum(value), count=n(), sd=sd(value)) %>% 
  mutate(freq=round((sum/count)*100,2))

d.qual.graph$variable <- factor(d.qual.graph$variable, 
                                levels=c("method.or.variable.name.from.sample.page",
                                         "incorrect.variable.name.from.the.table..or.in.the.wrong.place.",
                                         "extra.characters", "extra.lines.of.code", "raw.numerical.data" ), 
                                labels=c("SP","IV", "EC","EL", "RD"))

qualerrors_graph <- ggplot(d.qual.graph, aes(x=reorder(variable, -freq), y=freq, fill=groupedmajor)) +
  geom_bar(stat='identity', position="dodge", colour="black") + 
  facet_grid(. ~ assignedstyle.x) + 
  labs(x="Error Category", y="Percentage", fill="Major") + 
  scale_fill_grey(start=0.6, end=0.9) + 
  theme(legend.position="bottom",text = element_text(size=24)) +
  ylim(0,50)

qualerrors_graph

ggsave("graphs/qualerrors_graph.eps", width=16, height=6)


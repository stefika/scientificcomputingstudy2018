#!/bin/python3

import pandas as pd
import re

df = pd.read_csv("events.csv") 
df['error_class'] = df.apply(lambda _: '', axis=1)
df['error_category'] = df.apply(lambda _: '', axis=1)
df['error_note'] = df.apply(lambda _: '', axis=1)

errorDict = {
        "Incorrect answer. Check": ["task error", "incorrect answer", "Output doesn't match expected output"],
        "unexpected '.*' in": ["syntax error", "unexpected symbol", "wrong symbol in some part of the program"],
        "object .* not found": ["semantic error", "unknown identifier", "calling a non-existing object or misspelling name, possibly calling column name as object"],
        "Binding not found": ["semantic error", "unknown identifier", "calling undefined object in tidyverse"],
        "unused argument": ["semantic error", "unused argument(s)", "adding of superfluous argument to the function call. ex: sd(df, FUN=sd) or using same argument twice or assigning column names to values ex: sd(aggregate, teamID=HR)"],
        "argument.*is missing": ["semantic error", "argument missing", "missing important argument in function call"],
        "could not find function": ["semantic error",  "function not defined", "calling non-existing functions. sometimes trying to define new functions apparently or misunderstanding piping in tidyverse"],
        "unexpected symbol": ["syntax error", "unexpected symbol", "error occured due to missed commas etc"],
        "no rows to aggregate": ["semantic error", "no rows to aggregate (wrong column name)", "this seems to be caused by participants concatenating columnnames after the $ operator and similar issues resulting in operations on empty columns. Ex: americanStats$tesamIDbyHR"],
        "must have both left and right hand side": ["semantic error", "wrong use of tilde operator (must have both sides)", "This error is caused by participants attempting to use the tilde operator without arguments on both sides of the binary operator"],
        "variable lengths differ": ["data error", "can't aggregate, variable lengths differ", "error with tilde operator using it on two variables with different lengths"],
        "undefined columns": ["data error", "undefined columns selected", "error in filtering columns ex: americanStats[americanStats$HR<20]"],
        "incorrect number of subscripts": ["syntax error",  "Incorrect number of subscripts on matrix", "error with using comma after filtering attempt. ex: americanStats$rating[americanStats$HAB >.3,]"],
        "unexpected input in": ["syntax error", " unexpected input", "appears in many different situations where wrong symbols are used or the right ones are skipped"],
        "'by' must be a list": ["type error", "'by' must be a list", "assigning a non-list object to the by argument in aggregate"],
        "not meaningful for factors": ["type error", "function not defined", "attempting to summarize a column with factors using numerical functions (i.e. sum on factors)"],
        "replacement has .* rows": ["semantic error", "replacement has different number of rows", "unexpected assignment of constants to (filtered) data frames"],
        "attempt to apply non-function": ["syntax error", "attempt to apply non-function", "attempting to call a non-function with arguments ex: americanStats$RBI(CHA)"],
        "arguments must have same length": ["semantic error", "arguments must have same length", "unclear"],
        "cannot.* coerce": ["type error","cannot coerce type to fit", "can not coerce a type into another type as instructed"],
        "object of type .* is not subsettable": ["type error", "object is not subsettable", "attempting to select column on object that does not have columns, such as a function ex: aggregate$americanStats"],
        "is not a .*":  ["type error", "type is not right type for argument", "example occurence when trying to use a column as a function for FUN argument"],
        "must be.*, not .*": ["type error", "condition not correct type","seems to have mostly occured with wrong use of if_else function"],
        "differing number of rows": ["semantic error", "differing number of rows", "error in connection with calling data wrong in transform method"],
        "operator": ["syntax error", "wrong operator", "wrong use of different operators"],
        "unexpected": ["syntax error" , "unexpected input", "all kinds of weird input errors"],
        "subscript": ["syntax error", "faulty subscript(s)", "wrong use of tilde or generally confusing use of subscripts for column names"],
        "is of unsupported type": ["type error", "function as column name", "appears to have been a misconception in which participants tried to use the summarize function by calling it on a function instead of a column ex: summarize(sd)"],
        "invalid 'type'": ["type error", "use of function as parameter", "use of a function as parameter where a column was expected" ],
        "is possible only for .* types": ["type error", "returned function", "attempted to return a function as the answer for the task"],
        "only defined on a data frame": ["type error", "function not defined", "attempt to call a numeric aggregation function on a dataframe with non-numerical columns"],
        "data must be an environment or data.frame": ["type error", "must me environment or dataframe", "attempt to call tilde function without a data frame"],
        "'data' argument is of the wrong type": ["semantic error", "data not assigned", "error is that data argument is not assigned and therefore tries to assign one of the unnamed arguments to data (which are typically functions)"],
        "applies only to vectors": ["type error", "only applies to vectors", "unclear, only appears once"],
        "Expecting a single value": ["type error", "parameter mismatch, expecting single value", "seems to have to do with group_by and summarize"],
        "can't be modified because it's a grouping": ["semantic error", "can't modify grouping variable", "attempting to summarize the grouping variable"],
        "attempt to replicate an object of type": ["type error", "attempt to assign function to column", "participants trying to assign mean to a new column"],
        "Invalid formula specification": ["semantic error", "invalid formula, too many slots", "seems to happen when people attempt to use a formula as unary operator"],
        "invalid term in model formula": ["semantic error", "invalid formula, invalid term", "appears to happen in the case that someone uses a constant number in formula"],
        "invalid.*left side of assignment": ["syntax error", "assignment of number to function", "someone tried to assign their numerical calculation to a function ex: meanRBI()= (102+81+56+88+19+25+65+90)/8;"],
        "invalid.*left-hand side": ["syntax error", "assignment of column to constant", "attempt to assign a column to a constant number, assuming they misunderstood the arrow <-"],
        "target of assignment expands to non-language object": ["syntax error", "assignment to non-language object", "wild attempts at assignment" ],
        "'trim' must be numeric": ["semantic error", "wrong use of mean", "attempt to call mean on two columns at once?"],
        "Column.*is unknown": ["type error", "unknown column", "attempt to call group_by with anything other than a column"],
        "argument is not interpretable": ["type error", "argument not interpretable", "attempt to call sd with wrong type"],
        "argument is of length zero": ["semantic error", "argument empty", "attempt to call sd on two columns at once?"],
        "is not TRUE": ["semantic error", "constraint error", "unclear"],
        "filter condition does not evaluate to a logical vector": ["type error", "filter condition not logical", "filter condition was a single column or something"], 
        "incorrect number of dimensions": ["syntax error", "wrong dimensions", "attempt to index filtered column with wrong number of dimensions"],
        "no applicable method for.*applied to an object": ["type error", "method not found", "attempt to call summarize on a function (typically these are instances where people wildly try to call stuff with parameters without using functions ex: table%>\%summarize(americanStats(AB)))"],
        "cannot open file": ["task error", "can't open file", "someone trying to make a plot from the data for no reason (id=111)"],
        "only defined for equally-sized": ["syntax error", "unequally sized inputs", "someone forgot the - in <- and the interpreter thought it was a comparison"],
        "Result must have length": ["semantic error", "length mismatch", "someone tried to apply the result of a filter to a column (?) and it triggered this error ex: HR<-americanStats%>\%filter(mpg>20)"]
        }


for i, row in df.iterrows():
    for key, value in errorDict.items():
        found = re.search(key, row['output'])
        if found:
            df.at[i,'error_category'] = value[0]
            df.at[i,'error_class'] = value[1]
            df.at[i,'error_note'] = value[2]
            continue

df.to_csv('quant.csv')
READ.ME for replication packet for Randomized Controlled Trial on the Wild Wild West ofScientific Computing with Student Learners

revision 06-2019

The folder contents are as follows:


	2019ScientificComputing.zip 
		this is the experiment folder as it was run on the server. This zip also contains the tasks, instruction sheets (as used in the actual experiment), and the solution scripts that were concatinated to participants code so they could be checked. 

	corrections - this folder contains corrected instruction sheets.

	dataAnalysis - this folder contains the following r files:
		DataAnalysis.r - this script organizes the cleaned data and creates the graph "ComparisonofTaskCompletionTimes.eps"
		it also calculates several descriptive statistics. 
		finalCodeClean.r - this code cleans the data file allData.csv as much as possible. (NOTE: some manual coding is needed to create the complete cleaned csv)

		Also it contains QualitativeReview.csv - this is the qualitative data that has been anonimized and coded. 
				RandomSampleandCalculationsforKappa.xlsx - this is the spreadsheet for the Kappa calculations.

	graphs - this folder contains the full size eps graphs used in the paper

	quantitativeAnalysisReplication.zip - this zip file contains the scripts for running the qualitative analysis.

	tasksInstructionsandSolutions - this folder contains the tasks and the solutions to the tasks

